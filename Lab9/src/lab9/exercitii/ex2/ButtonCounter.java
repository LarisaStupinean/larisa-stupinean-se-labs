package lab9.exercitii.ex2;

import lab9.exemple.exemplu3.ButtonAndTextField2;

import javax.swing.*;
import java.awt.event.*;

public class ButtonCounter extends JFrame {

    JLabel counter;
    JTextField tCounter;
    JButton button;
    int i = 0;

    ButtonCounter() {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);
        int width = 100;
        int height = 20;

        counter = new JLabel("Counter ");
        counter.setBounds(10, 50, width, height);

        tCounter = new JTextField();
        tCounter.setBounds(70, 50, width, height);
        tCounter.setText(""+i);

        button = new JButton("Increment");
        button.setBounds(10, 150, width, height);

        button.addActionListener(new ButtonListener());

        add(counter);
        add(tCounter);
        add(button);
    }

    public static void main(String[] args) {
        new ButtonCounter();
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
                i++;
                tCounter.setText(""+i);
        }
    }
}


