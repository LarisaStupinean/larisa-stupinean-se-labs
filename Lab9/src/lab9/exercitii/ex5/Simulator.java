package lab9.exercitii.ex5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class Simulator extends JFrame {
    static int i = 0;
    int steps = 3;
    ArrayList<JTextField> stationList = new ArrayList<JTextField>();
    static ArrayList<Controller> l_controllers = new ArrayList<Controller>();
    JButton start_button;
    JButton add_button;

    Simulator(ArrayList<Controller> list_controllers) {
        l_controllers = list_controllers;
        setTitle("Simulator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init(list_controllers);
        setSize(700, 300);
        setVisible(true);
        setResizable(false);
    }

    public void start() {
        System.out.println("\nStart train control\n");
        for (int i = 0; i < 3; i++) {
            System.out.println("### Step " + i + " ###");
            new Simulator(l_controllers);
            for (Controller cc : l_controllers) {
                cc.controlStep();
            }

            System.out.println();

            for (Controller cc : l_controllers) {
                cc.displayStationState();
            }
        }
    }

    public void init(ArrayList<Controller> list_controlers) {
        this.setLayout(null);
        int width = 120;
        int height = 20;
        int i = 0;
        int base_x = 50;
        int x = 50;
        int y = 20;
        for (Controller c : list_controlers) {
            JTextField aux = new JTextField();
            aux.setBounds(x, y, width, height);
            aux.setEditable(false);
            aux.setText(c.stationName);
            add(aux);

            for (Segment s : c.list) {
                JTextField aux1 = new JTextField();
                aux1.setBounds(x + width + 10, y, width, height);
                aux1.setEditable(false);
                if (s.hasTrain())
                    aux1.setText("ID=" + String.valueOf(s.id) + "_Train=" + s.getTrain().name);
                else
                    aux1.setText("");
                add(aux1);
                x += 130;
            }
            stationList.add(aux);
            x = base_x;
            y += 30;
            i++;
        }
        start_button = new JButton("Start Simulator");
        start_button.setBounds(50, 200, 120, 30);

        start_button.addActionListener(new StartSim());

        add_button = new JButton("Add train");
        add_button.setBounds(190, 200, 120, 30);

        add_button.addActionListener(new AddTrain());

        Random rand = new Random();

        int redValue = rand.nextInt(255);
        int greenValue = rand.nextInt(255);
        int blueValue = rand.nextInt(255);

        Color clr = new Color(redValue, greenValue, blueValue);

        start_button.setBackground(clr);

        redValue = rand.nextInt(255);
        greenValue = rand.nextInt(255);
        blueValue = rand.nextInt(255);

        clr = new Color(redValue, greenValue, blueValue);

        add_button.setBackground(clr);

        add(start_button);
        add(add_button);
    }

    class StartSim implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            start();
        }
    }

    class AddTrain implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            new Add(l_controllers);
        }
    }


    public static void main(String[] args) {
        //build station Cluj-Napoca
        Controller c1 = new Controller("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controller c2 = new Controller("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station Bistrita
        Controller c3 = new Controller("Bistrita");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers
        c1.addNeighbourController(c2);
        c1.addNeighbourController(c3);
        c2.addNeighbourController(c1);
        c2.addNeighbourController(c3);
        c3.addNeighbourController(c1);
        c3.addNeighbourController(c2);

        //testing
        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca", "R-002");
        s6.arriveTrain(t2);

        Train t3 = new Train("Bistrita", "RE-015");
        s4.arriveTrain(t3);

        Train t4 = new Train("Cluj-Napoca", "R-062");
        s9.arriveTrain(t4);

        Train t5 = new Train("Bucuresti", "IC-100");
        s8.arriveTrain(t5);

        Train t6 = new Train("Bistrita", "RE-788");
        s2.arriveTrain(t6);

        ArrayList<Controller> list_controllers = new ArrayList<Controller>();
        list_controllers.add(c1);
        list_controllers.add(c2);
        list_controllers.add(c3);
        //list_controllers.add(new Controller("da"));

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        new Simulator(list_controllers);

//        c1.displayStationState();
//        c2.displayStationState();
//        c3.displayStationState();
//
//        System.out.println("\nStart train control");
//
//        //execute 3 times controller steps
//        for(int i=0; i<3; i++){
//            System.out.println("\n### Step "+i+" ###");
//            c3.controlStep();
//            c2.controlStep();
//            c1.controlStep();
//
//            System.out.println();
//
//            c1.displayStationState();
//            c2.displayStationState();
//            c3.displayStationState();
    }
}
