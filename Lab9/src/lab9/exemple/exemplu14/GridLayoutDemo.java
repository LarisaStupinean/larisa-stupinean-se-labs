package lab9.exemple.exemplu14;

//Pozitionarea componentelor folosind gestionari de pozitionare. Gestionarul GridLayout

import javax.swing.*;
import java.awt.*;

public class GridLayoutDemo extends JFrame {
    public GridLayoutDemo(){
        setLayout(new GridLayout(3, 2));
        add(new JButton("Button 1"));
        add(new JButton("Button 2"));
        add(new JButton("Button 3"));
        add(new JButton("Long-Named Button 4"));
        add(new JButton("5"));

        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new GridLayoutDemo();
    }
}
