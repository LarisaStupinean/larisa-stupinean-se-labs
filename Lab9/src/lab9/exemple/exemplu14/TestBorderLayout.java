package lab9.exemple.exemplu14;

//Pozitionarea componentelor folosind gestionari de pozitionare. Gestionarul BorderLayout

import javax.swing.*;
import java.awt.*;

public class TestBorderLayout {
    public static void main(String[] args) {
        JFrame f = new JFrame("Border Layout");
        f.setLayout(new BorderLayout()); //poate sa lipseasca

        f.add(new Button("North"), BorderLayout.NORTH);
        f.add(new Button("South"), BorderLayout.SOUTH);
        f.add(new Button("East"), BorderLayout.EAST);
        f.add(new Button("West"), BorderLayout.WEST);
        f.add(new Button("Center"), BorderLayout.CENTER);
        f.pack();
        f.setVisible(true);
    }
}
