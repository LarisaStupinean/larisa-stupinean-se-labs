package lab9.exemple.exemplu14;

//Pozitionarea componentelor folosind gestionari de pozitionare. Gestionarul FlowLayout

import javax.swing.*;
import java.awt.*;

public class FlowLayoutDemo extends JFrame {
    public FlowLayoutDemo(String s){
        setLayout(new FlowLayout());
        setTitle(s);
        add(new JButton("Button 1"));
        add(new JButton("Button 2"));
        add(new JButton("Button 3"));
        add(new JButton("Long-Named Button 4"));
        add(new JButton("5"));

        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new FlowLayoutDemo("Flow demo");
    }
}
