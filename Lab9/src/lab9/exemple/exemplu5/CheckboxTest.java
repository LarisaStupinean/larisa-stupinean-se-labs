package lab9.exemple.exemplu5;

//Casute de selectie CheckBox

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class CheckboxTest extends JFrame implements ItemListener {
    private JLabel label1, label2;
    private JCheckBox cbx1, cbx2, cbx3;

    public CheckboxTest(String title){
        super(title);
        initialize();
        setVisible(true);
    }

    public void initialize() {
        setLayout(new GridLayout(5,1));
        label1 = new JLabel("Components: ");
        label2 = new JLabel("");
        cbx1 = new JCheckBox("component 1");
        cbx2 = new JCheckBox("component 2");
        cbx3 = new JCheckBox("component 3");

        add(label1);
        add(label2);
        add(cbx1);
        add(cbx2);
        add(cbx3);
        pack();
        setSize(200,200);

        cbx1.addItemListener(this);
        cbx2.addItemListener(this);
        cbx3.addItemListener(this);
    }

    //metoda intefetei ItemListener
    public void itemStateChanged(ItemEvent e){
        StringBuffer ingredients = new StringBuffer();
        if(cbx1.isSelected()==true)
            ingredients.append( " component 1");
        if(cbx2.isSelected()==true)
            ingredients.append( " component 2");
        if(cbx3.isSelected()==true)
            ingredients.append( " component 3");
        label2.setText(ingredients.toString());
    }

    public static void main(String[] args) {
        new CheckboxTest("title");
    }
}
