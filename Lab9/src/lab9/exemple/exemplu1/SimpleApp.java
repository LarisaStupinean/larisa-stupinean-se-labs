package lab9.exemple.exemplu1;

//Construire fereastra principala aplicatie

import javax.swing.*;

public class SimpleApp extends JFrame {

    SimpleApp(){
        setTitle("Window's Title");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        setVisible(true);
    }

    public static void main(String[] args) {
        SimpleApp a = new SimpleApp();
    }
}
