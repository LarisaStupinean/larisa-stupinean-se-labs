package lab9.exemple.exemplu11;

//Desenarea si afisarea imaginilor Exemplu 1

import javax.swing.*;
import java.awt.*;

//Example demonstrating drawPolyline()
public class SinExample extends JFrame {
    public SinExample(){
        this.setSize(new Dimension(300,200));
        int width = getSize().width;
        int height = getSize().height;

        int num_points = 21;

        //Create an instance of DrawingPanel
        Polygon1Panel polygon1Panel = new Polygon1Panel(width,height,num_points);
        //Add the DrawingPanel to the contentPane
        add(polygon1Panel);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new SinExample();
    }
}
