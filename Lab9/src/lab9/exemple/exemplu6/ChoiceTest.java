package lab9.exemple.exemplu6;

//Casute de selectie JComboBox

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class ChoiceTest extends JFrame implements ItemListener {
    private JLabel label;
    private JComboBox colors;

    public ChoiceTest(String title){
        super(title);
        initialize();
        setVisible(true);
    }

    public void initialize(){
        setLayout(new GridLayout(4,1));

        label = new JLabel("Choose the color: ");
        label.setOpaque(true);
        label.setBackground(Color.red);

        colors = new JComboBox();
        colors.addItem("Red");
        colors.addItem("Green");
        colors.addItem("Blue");
        colors.setSelectedIndex(0);

        add(label);
        add(colors);
        pack();
        setSize(200,100);

        colors.addItemListener(this);
    }

    //metoda intefetei ItemListener
    public void itemStateChanged(ItemEvent e){
        switch(colors.getSelectedIndex()){
            case 0:
                label.setBackground(Color.red);
                break;
            case 1:
                label.setBackground(Color.green);
                break;
            case 2:
                label.setBackground(Color.blue);
        }
    }

    public static void main(String[] args) {
        new ChoiceTest("Test");
    }
}
