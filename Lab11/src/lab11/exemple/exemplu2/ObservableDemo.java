package lab11.exemple.exemplu2;

//Sablonul de proiectare observer. Implementare utilizand clasele Observer si Observable din pachetul java

import java.util.Observable;
import java.util.Observer;

public class ObservableDemo implements Observer {
    public static void main(String[] args) {
        //create watched and watcher objects
        ObservedObject watched = new ObservedObject("Original Value");
        //watcher object listens to object change
        ObservableDemo watcher = new ObservableDemo();

        //trigger value change
        watched.setValue("New Value");

        //add observer to the watched object
        watched.addObserver(watcher);

        //trigger value change
        watched.setValue("Latest Value");

        watched.setValue("Last Value");
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Update called with Arguments: "+arg);
    }
}
