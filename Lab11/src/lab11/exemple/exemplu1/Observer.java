package lab11.exemple.exemplu1;

interface Observer {
    public abstract void update(Object event);
}
