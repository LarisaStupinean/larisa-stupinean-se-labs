package lab11.exercitii.ex2;

public class Product {
    private String name;
    private int quantity;
    private double price;

    Product(String name, int quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity >= 0)
            this.quantity = quantity;
    }

    public boolean isEqual(Product p) {
        return this.name.equals(p.name) && this.quantity == p.quantity && this.price == p.price;
    }

    @Override
    public String toString() {
        return "Product => [Name: " + name + "; Quantity: " + quantity + "; Price: " + price + "]\n";
    }
}
