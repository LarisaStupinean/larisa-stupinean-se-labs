package lab11.exercitii.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class StockController {
    private Products products;
    private Window win;

    StockController(Products products, Window win) {
        products.addObserver(win);
        this.products = products;
        this.win = win;

        win.addNewProdListener(new NewProdListener());
        win.addViewAvProdsListener(new ViewAvProdsListener());
        win.addDeleteProdListener(new DeleteProdListener());
        win.addChgQtyListener(new ChgQtyListener());
    }

    class NewProdListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ChooseProdWindow chWin = new ChooseProdWindow("Add new product", "Add product");
            chWin.getButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    products.addProduct(chWin.getProduct());
                    chWin.dispatchEvent(new WindowEvent(chWin, WindowEvent.WINDOW_CLOSING));
                }
            });
        }
    }

    class ViewAvProdsListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            products.viewAvProducts();
        }
    }

    class DeleteProdListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ChooseProdWindow chWin = new ChooseProdWindow("Delete product", "Delete product");
            chWin.getButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Product chosenProduct = chWin.getProduct();
                    boolean validProd = false;
                    try {
                        for (Product p : products.getProducts())
                            if (chosenProduct.isEqual(p)) {
                                products.deleteProduct(p);
                                validProd = true;
                            }
                        if (!validProd)
                            JOptionPane.showMessageDialog(null, "This product does not exist!", "Error!", JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception exception) {
                    }
                    chWin.dispatchEvent(new WindowEvent(chWin, WindowEvent.WINDOW_CLOSING));
                }
            });
        }
    }

    class ChgQtyListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ChangeQuantityWindow chgWin = new ChangeQuantityWindow("Change quantity of product", "Change quantity");
            chgWin.getButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Product chosenProduct = chgWin.getProduct();
                    boolean validProd = false;
                    for (Product p : products.getProducts())
                        if (chosenProduct.isEqual(p)) {
                            products.changeQuantity(p, chgWin.getNewQuantity());
                            validProd = true;
                        }
                    if (!validProd)
                        JOptionPane.showMessageDialog(null, "This product does not exist!", "Error!", JOptionPane.INFORMATION_MESSAGE);
                    chgWin.dispatchEvent(new WindowEvent(chgWin, WindowEvent.WINDOW_CLOSING));
                }
            });
        }
    }
}
