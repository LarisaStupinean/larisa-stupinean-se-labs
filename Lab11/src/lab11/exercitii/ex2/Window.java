package lab11.exercitii.ex2;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class Window extends JFrame implements Observer {
    private JButton addProdButton;
    private JButton viewProdsButton;
    private JButton deleteProdButton;
    private JButton chgQuantityButton;
    private JTextArea display;

    Window(String name){
        int height = 20;
        setTitle(name);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(450, 500);
        setLocationRelativeTo(null);
        setLayout(null);

        addProdButton = new JButton("Add new product");
        addProdButton.setBounds(10,10,130, height);

        viewProdsButton = new JButton("View available products");
        viewProdsButton.setBounds(150,10,200, height);

        deleteProdButton = new JButton("Delete product");
        deleteProdButton.setBounds(10,40,130, height);

        chgQuantityButton = new JButton("Change product's available quantity");
        chgQuantityButton.setBounds(150,40,250, height);

        display = new JTextArea();
        display.setBounds(10,70,420,390);

        add(addProdButton);
        add(viewProdsButton);
        add(deleteProdButton);
        add(chgQuantityButton);
        add(display);
        setVisible(true);
    }


    @Override
    public void update(Observable o, Object arg) {
        this.display.setText(""+arg);
    }

    public void addNewProdListener(StockController.NewProdListener listener) {
        addProdButton.addActionListener(listener);
    }

    public void addViewAvProdsListener(StockController.ViewAvProdsListener listener) {
        viewProdsButton.addActionListener(listener);
    }

    public void addDeleteProdListener(StockController.DeleteProdListener listener) {
        deleteProdButton.addActionListener(listener);
    }

    public void addChgQtyListener(StockController.ChgQtyListener listener) {
        chgQuantityButton.addActionListener(listener);
    }
}
