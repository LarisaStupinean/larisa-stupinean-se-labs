package lab11.exercitii.ex2;

import java.util.ArrayList;
import java.util.Observable;

public class Products extends Observable {
    ArrayList<Product> products = new ArrayList<>();

    public void addProduct(Product prod) {
        products.add(prod);
        setChanged();
        notifyObservers("New product added:\n" + prod + "\n\nAll products:\n" + products);
    }

    public void deleteProduct(Product prod) {
        products.remove(prod);
        setChanged();
        notifyObservers("Product deleted: \n" + prod + "\n\nAll products:\n" + products);
    }

    public void viewAvProducts() {
        ArrayList<Product> avProds = new ArrayList<>();
        for (Product p : products)
            if (p.getQuantity() != 0)
                avProds.add(p);
        setChanged();
        notifyObservers("Available products: \n" + avProds + "\n\nAll products:\n" + products);
    }

    public void changeQuantity(Product prod, int quantity) {
        prod.setQuantity(quantity);
        setChanged();
        notifyObservers("Quantity changed: \n" + prod + "\n\nAll products:\n" + products);
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
}
