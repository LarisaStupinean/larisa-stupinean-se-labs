package lab11.exercitii.ex2;

import javax.swing.*;
import java.awt.*;

public class ChangeQuantityWindow extends JFrame {
    private JButton button;
    private JTextField name;
    private JTextField quantity;
    private JTextField price;
    private JTextField newQuantity;

    ChangeQuantityWindow(String title, String nameButton) {
        setLocationRelativeTo(null);
        setLayout(new GridLayout(7, 2));

        button = new JButton(nameButton);
        name = new JTextField();
        quantity = new JTextField();
        price = new JTextField();
        newQuantity = new JTextField();

        add(new JLabel("Choose the product: "));
        add(new JLabel());
        add(new JLabel("Name"));
        add(name);
        add(new JLabel("Quantity"));
        add(quantity);
        add(new JLabel("Price"));
        add(price);
        add(new JLabel("New quantity"));
        add(newQuantity);
        add(new JLabel());
        add(button);

        pack();
        setVisible(true);
    }

    public JButton getButton() {
        return button;
    }

    public Product getProduct() {
        String name = "";
        int quantity = 0;
        double price = 0;

        if(!this.name.getText().isEmpty())
            name = this.name.getText();
        try {
            if (!this.quantity.getText().isEmpty())
                quantity = Integer.parseInt(this.quantity.getText());
        } catch (NumberFormatException e) {
            System.out.println("String cannot be converted to integer!!!");
        }

        try {
            if (!this.price.getText().isEmpty())
                price = Double.parseDouble(this.price.getText());
        } catch (NumberFormatException e) {
            System.out.println("String cannot be converted to double!!!");
        }
        return new Product(name, quantity, price);
    }

    public int getNewQuantity() {
        int newQuantity = 0;
        try {
            if (!this.newQuantity.getText().isEmpty())
                newQuantity = Integer.parseInt(this.newQuantity.getText());
        } catch (NumberFormatException e) {
            System.out.println("String cannot be converted to integer!!!");
        }
        return newQuantity;
    }
}
