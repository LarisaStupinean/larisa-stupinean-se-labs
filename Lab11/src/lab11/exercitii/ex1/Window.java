package lab11.exercitii.ex1;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class Window extends JFrame implements Observer {
    private JLabel label;
    private JTextField temp;

    Window() {
        setTitle("Temperature Monitor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(190, 150);
        setLocationRelativeTo(null);
        setLayout(null);

        label = new JLabel("Temperature:");
        label.setBounds(50, 30, 110, 20);

        temp = new JTextField();
        temp.setBounds(65, 60, 50, 20);
        temp.setCaretColor(Color.WHITE);

        add(label);
        add(temp);
        this.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.temp.setText("   " + arg + "°C");
    }
}
