package lab11.exercitii.ex1;

import java.util.Observable;

class TemperatureSensor extends Observable implements Runnable {
    private int value;

    public void readValue() {
        int val = (int) (Math.random() * 100);
        if(this.value!=val){
            this.value = val;
            setChanged();
            notifyObservers(val);
        }

    }

    @Override
    public void run() {
        while (true) {
            readValue();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
