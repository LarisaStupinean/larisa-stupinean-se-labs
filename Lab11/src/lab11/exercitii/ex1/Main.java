package lab11.exercitii.ex1;

public class Main {
    public static void main(String[] args) {
        Window win = new Window();
        TemperatureSensor tempSensor = new TemperatureSensor();
        Thread thread = new Thread(tempSensor, "Sensor Thread");
        tempSensor.addObserver(win);
        thread.start();
    }
}
