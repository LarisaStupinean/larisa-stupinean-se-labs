package lab5.exemple.exemplu2;

public class Muzica { //clasa principala
    static void play(Instrument i){
        //metoda statica care porneste un instrument generic ce implementeaza interfata Instrument
        i.play();
    }
    static void playAll(Instrument[] e){
        for(int i=0; i<e.length; i++)
            play(e[i]);
    }

    public static void main(String[] args) {
        Instrument[] orchestra = new Instrument[2];
        int i=0;
        orchestra[i++] = new Pian();
        orchestra[i++] = new Vioara();
        playAll(orchestra);
    }
}
