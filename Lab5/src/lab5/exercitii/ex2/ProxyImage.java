package lab5.exercitii.ex2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private String mode;

    public ProxyImage(String fileName, String mode){
        this.fileName = fileName;
        if((mode == "real")||(mode == "rotated"))
            this.mode = mode;
        else System.out.println("Mode can be only 'real' or 'rotated'!!");
    }

    public void display(){
        if(mode == "real") {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else {
            if(rotatedImage == null)
                rotatedImage = new RotatedImage(fileName);
            rotatedImage.display();
        }
    }
}
