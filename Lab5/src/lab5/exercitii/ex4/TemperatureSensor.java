package lab5.exercitii.ex4;

public class TemperatureSensor extends Sensor{
    TemperatureSensor(String location){
        super(location);
    }
    public int readValue() {
        return (int) (Math.random() * 100);
    }
}
