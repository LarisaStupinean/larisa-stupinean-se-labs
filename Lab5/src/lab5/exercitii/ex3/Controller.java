package lab5.exercitii.ex3;

public class Controller {
    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;
    Controller(TemperatureSensor tempSensor, LightSensor lightSensor){
        this.tempSensor = tempSensor;
        this.lightSensor = lightSensor;
    }
    public void control(){
        for(int i=0; i<20;i++) {
            int value_temp = tempSensor.readValue();
            int value_light = lightSensor.readValue();
            System.out.println("The temperature at second " + i + " is " + value_temp);
            System.out.println("The light at second " + i + " is " + value_light + "\n");
            try {
                Thread.sleep(1000);
            }catch (InterruptedException ignored) {}
        }
    }
}

