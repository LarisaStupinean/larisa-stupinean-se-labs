package lab5.exercitii.ex3;

public class LightSensor extends Sensor {
    LightSensor(String location){
        super(location);
    }
    public int readValue(){
        return (int)(Math.random()*100);
    }
}
