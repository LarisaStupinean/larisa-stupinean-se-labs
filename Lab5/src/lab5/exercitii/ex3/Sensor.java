package lab5.exercitii.ex3;

abstract class Sensor {
    private String location;
    Sensor(String location){
        this.location = location;
    }
    public abstract int readValue();
    public String getLocation(){
        return "The location of the Sensor is "+location;
    }
}
