package lab5.exercitii.ex1;

import lab5.exemple.exemplu1.Goose;
import lab5.exemple.exemplu1.Penguin;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(5);
        shapes[1] = new Rectangle(2,3);
        shapes[2] = new Square(4);
        for(int i=0; i<shapes.length; i++) {
            System.out.println(shapes[i].toString());
            System.out.println("Area = "+shapes[i].getArea()+"\tPerimeter = "+shapes[i].getPerimeter()+"\n");
        }
    }
}