package lab7.exercitii.ex4;

import java.io.Serializable;

public class Car implements Serializable {
    String model;
    double price;

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }
    public String toString(){
        return "[car-> model: "+this.model+"; price: "+this.price+"]";
    }
}
