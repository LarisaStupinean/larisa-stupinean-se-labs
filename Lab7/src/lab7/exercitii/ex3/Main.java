package lab7.exercitii.ex3;

//we put as arguments from command line first "encrypt Data.txt", then "decrypt Data.enc"

public class Main {
    public static void main(String[] args) {
        Encrypt en = new Encrypt();

        if(args[0].equalsIgnoreCase("encrypt")) {
            en.encryptFile(args[1]);
            System.out.println("Operation completed");
        }
        else if(args[0].equalsIgnoreCase("decrypt")) {
            en.decryptFile(args[1]);
            System.out.println("Operation completed");
        }
        else System.out.println("Invalid operation!");    }
}

