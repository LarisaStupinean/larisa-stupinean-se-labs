package lab7.exercitii.ex3;

import java.io.*;

public class Encrypt {
    void encryptFile(String source){
        try {
            BufferedReader in = new BufferedReader(new FileReader(source));

            int point = source.indexOf('.');
            String outFilename = source.substring(0, point) + ".enc";
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFilename)));


            int c=0;
            while((c=in.read())!=-1) {
                char ch = (char) (c << 1);
                out.print(ch);
            }
            in.close();
            out.close();
        } catch (IOException e) {
            System.out.println("Error encrypting file:" + e.getMessage());
        }
    }

    void decryptFile(String source) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(source));

            int point = source.indexOf('.');
            String outFilename = source.substring(0, point) + ".dec";
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFilename)));

            int c=0;
            while ((c=in.read())!=-1) {
                char ch = (char) (c >> 1);
                out.print(ch);
            }
            in.close();
            out.close();
        } catch (IOException e) {
            System.out.println("Error decrypting file:" + e.getMessage());
        }
    }
}
