package lab7.exemple.exemplu8;

import java.io.Serializable;

class Engine implements Serializable {
    String tip;
    public Engine(String t){
        tip = t;
    }
    public String toString(){
        return "-"+tip+"-";
    }
}
