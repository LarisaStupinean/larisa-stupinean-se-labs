package lab7.exemple.exemplu8;

import java.io.*;
import java.util.*;

class Tren implements Serializable {
    LinkedList t = new LinkedList();
    transient int id;

    Tren(){
        id = (int)(Math.random()*1000);
    }

    void addVagon(Vagon v){
        t.addLast(v);
    }

    void addLocomotiva(Locomotiva e){
        t.addFirst(e);
    }

    void save(String fileName){
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Tren salvat in fisier");
        } catch (IOException e){
            System.err.println("Trenul nu poate fi scris in fisier");
            e.printStackTrace();
        }
    }

    static Tren load(String fileName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        Tren t = (Tren)in.readObject();
        return t;
    }

    public String toString(){
        String x = "Tren ID="+id+" ";
        for(Iterator i = t.iterator(); i.hasNext();){
            Object element = (Object) i.next();
            x+=element;
        }
        return x;
    }
}
