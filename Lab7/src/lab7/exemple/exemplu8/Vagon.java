package lab7.exemple.exemplu8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

class Vagon implements Externalizable {
    int nr;
    int nrPasageri;

    public Vagon(){}
    public Vagon(int i, int p){
        nr = i;
        nrPasageri = p;
    }

    public String toString(){
        return "<"+nr+" pasageri="+nrPasageri+">";
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException{
        nr = in.readInt();
        nrPasageri = in.readInt();
    }

    public void writeExternal(ObjectOutput out) throws IOException{
        out.writeInt(nr);
        out.writeInt(nrPasageri);
    }
}
