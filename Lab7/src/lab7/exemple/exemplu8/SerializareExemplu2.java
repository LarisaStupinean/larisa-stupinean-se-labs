package lab7.exemple.exemplu8;

//Serializarea obiectelor

import java.util.*;
import java.io.*;

public class SerializareExemplu2 {
    public static void main(String[] args) {
        Tren tr = new Tren();
        Locomotiva l = new Locomotiva("XYZ", new Engine("diesel"));
        Vagon v1 = new Vagon(1,20);
        Vagon v2 = new Vagon(2,89);
        Vagon v3 = new Vagon(3,53);

        tr.addVagon(v1); tr.addVagon(v2); tr.addVagon(v3);
        tr.addLocomotiva(l);

        System.out.println(tr);

        tr.save("trenX");

        try {
            Tren t2 = Tren.load("trenX");
            System.out.println(t2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
