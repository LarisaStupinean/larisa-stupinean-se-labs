package lab7.exemple.exemplu8;

import java.io.Serializable;

class Locomotiva implements Serializable {
    String marca;
    Engine e;

    /*
    @param marca
    @param e
     */
    public Locomotiva(String marca, Engine e){
        this.marca = marca;
        this.e = e;
    }

    public String toString(){
        return "[Locomotiva"+marca+" "+e+"]";
    }

}
