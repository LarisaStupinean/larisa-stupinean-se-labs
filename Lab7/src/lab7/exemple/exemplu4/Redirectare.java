package lab7.exemple.exemplu4;

//Fluxuri standard de intrare/iesire

import java.io.*;

public class Redirectare {
    public static void main(String[] args) {
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(".project"));
            PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("test.out")));
            System.setIn(in);
            System.setOut(out);
            System.setErr(out);

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s;
            while((s = br.readLine())!=null)
                System.out.println(s);
            out.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
