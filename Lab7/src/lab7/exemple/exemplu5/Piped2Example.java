package lab7.exemple.exemplu5;

//Fluxuri de tip pipe

import java.io.PipedReader;
import java.io.PipedWriter;

public class Piped2Example {
    public static void main(String[] args) {
        try {
            PipedReader in = new PipedReader();
            PipedWriter out = new PipedWriter();
            in.connect(out);

            //scrie date in pipe
            out.write("Mesaj scris in pipe");

            //citeste din pipe
            while (in.ready()) {
                int x = in.read();
                System.out.println("Read from pipe: "+(char)x);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
