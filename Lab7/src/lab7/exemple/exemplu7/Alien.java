package lab7.exemple.exemplu7;

import java.io.*;

class Alien implements Serializable {
    String name;
    transient int id;

    public Alien(String n){
        this.name = n;
        id = (int)(Math.random()*100);
    }

    public void move(){
        System.out.println("Alien is moving."+this);
    }
    public String toString(){
        return "[alien="+name+":id="+id+"]";
    }
}
