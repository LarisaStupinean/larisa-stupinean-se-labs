package lab7.exemple.exemplu7;

import java.io.*;

class AlienFactory {
    Alien createAlien(String name) {
        Alien z = new Alien(name);
        System.out.println(z+" is alive.");
        return z;
    }

    void freezeAlien(Alien a, String storeRecipientName) throws IOException{
        ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(storeRecipientName));

        o.writeObject(a);
        System.out.println(a+":I'll be back.");
    }

    Alien unfreezeAlien(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(storeRecipientName));
        Alien x = (Alien)in.readObject();
        System.out.println(x+":I'm back.");
        return x;
    }
}
