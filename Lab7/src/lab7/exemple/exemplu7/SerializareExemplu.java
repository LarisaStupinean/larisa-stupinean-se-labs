package lab7.exemple.exemplu7;

//Serializarea obiectelor

public class SerializareExemplu {
    public static void main(String[] args) throws Exception{
        AlienFactory f = new AlienFactory();

        Alien a = f.createAlien("axx");
        Alien b = f.createAlien("abb");

        f.freezeAlien(a,"aliena.dat");
        f.freezeAlien(b,"alienb.dat");

        Alien x = f.unfreezeAlien("alienb.dat");
        Alien y = f.unfreezeAlien("aliena.dat");

        System.out.println(x);
        System.out.println(y);
    }
}
