package lab8.ex4;

public class FireSensor extends Sensor {
    boolean smoke=false;

    public FireSensor(String location){
        super(location);
    }

    public void readValue(boolean smoke) {
        this.smoke = smoke;
    }

    public boolean isSmoke() {
        return smoke;
    }
}