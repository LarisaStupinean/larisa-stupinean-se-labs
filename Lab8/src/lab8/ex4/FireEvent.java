package lab8.ex4;

class FireEvent extends Event {
    private boolean smoke;
    private FireSensor fireSensor;

    FireEvent(boolean smoke, FireSensor fireSensor) {
        super(EventType.FIRE);
        this.smoke = smoke;
        this.fireSensor = fireSensor;
    }

    boolean isSmoke(){
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + " detected by FireSensor in "+fireSensor.getLocation()+'}';
    }
}
