package lab8.ex4;

import java.io.*;
import java.util.*;

public class ControlUnit {
    TemperatureSensor temperatureSensor;
    ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    AlarmUnit alarmUnit;
    HeatingUnit heatingUnit;
    CoolingUnit coolingUnit;
    double presetTemperature = 21;

    private static ControlUnit controlUnit = null;

    private ControlUnit(TemperatureSensor t, ArrayList<FireSensor> f, AlarmUnit a, HeatingUnit h, CoolingUnit c) {
        this.temperatureSensor = t;
        this.fireSensors.addAll(f);
        this.alarmUnit = a;
        this.heatingUnit = h;
        this.coolingUnit = c;
    }

    public static ControlUnit getControlUnit(TemperatureSensor t, ArrayList<FireSensor> f, AlarmUnit a, HeatingUnit h, CoolingUnit c) {
        if (controlUnit == null)
            controlUnit = new ControlUnit(t, f, a, h, c);
        return controlUnit;
    }

    public static void save(String data) {
        try {
            FileWriter fw = new FileWriter("systems_logs.txt", true);
            fw.write(data + "\n");
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void setPresetTemperature(double temp) {
        this.presetTemperature = temp;
    }

    public void control(Event event) {
        if (event.getType().equals(EventType.FIRE)) {
            boolean fire = false;
            for (FireSensor fireSensor : fireSensors) {
                if (fireSensor.isSmoke() == true) {
                    fire = true;
                    break;
                }
            }
            if (fire == true) {
                System.out.println("Fire detected!");
                save("Fire detected!");
                alarmUnit.startAlarm();
                fire = false;
            }
        } else if (event.getType().equals(EventType.TEMPERATURE)) {
            if (temperatureSensor.getTemperature() > presetTemperature) {
                coolingUnit.cool(temperatureSensor.getTemperature(), presetTemperature);
                temperatureSensor.readValue(coolingUnit.temperature);
            } else if (temperatureSensor.getTemperature() < presetTemperature) {
                heatingUnit.heat(temperatureSensor.getTemperature(), presetTemperature);
                temperatureSensor.readValue(heatingUnit.temperature);
            } else {
                System.out.println("The temperature is equal to the preset temperature.");
                save("The temperature is equal to the preset temperature.");
            }
        }
    }
}