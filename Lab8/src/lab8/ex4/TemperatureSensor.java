package lab8.ex4;

class TemperatureSensor extends Sensor {
    double temperature = 21;

    public TemperatureSensor(String location){
        super(location);
        this.temperature = temperature;
    }

    public void readValue(double temperature){
        this.temperature = temperature;
    }

    public double getTemperature() {
        return temperature;
    }
}
