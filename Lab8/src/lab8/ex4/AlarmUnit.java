package lab8.ex4;

public class AlarmUnit {
    boolean status = false;
    GsmUnit gsmUnit = new GsmUnit();

    public void startAlarm(){
        status = true;
        ControlUnit.save("Alarm started.");
        System.out.println("Alarm started.");
        gsmUnit.call();
    }
    public void setStatus(boolean status){
        this.status = status;
    }
}
