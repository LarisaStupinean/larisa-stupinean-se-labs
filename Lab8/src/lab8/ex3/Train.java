package lab8.ex3;

class Train {
    String destination;
    String name;

    public Train(String destination, String name){
        super();
        this.destination = destination;
        this.name = name;
    }

    String getDestination(){
        return destination;
    }
}
