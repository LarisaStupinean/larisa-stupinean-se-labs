package lab12rec.ex2;

import java.io.*;

public class WriteFile extends Thread {
    private String text;

    WriteFile(String name, String text) {
        super(name);
        this.text = text;
    }

    @Override
    public void run() {
        File fout = new File("Test Exercise 2");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        try {
            bw.write(getName() + " has written:");
            bw.newLine();

            //for (int i = 0; i <= 10; i++) {
                bw.write(this.text);
                //bw.newLine();
                bw.close();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            //}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
