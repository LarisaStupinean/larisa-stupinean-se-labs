package lab12rec.ex2;

import java.io.*;

public class ReadFile extends Thread {
    private String text;

    ReadFile(String name) {
        super(name);
    }

    public String getText() {
        return text;
    }

    @Override
    public void run() {
        File fin = new File("Test Exercise 2");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fin);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(fis));

        this.text = getName() + " has read:\n";
        try {
            String line = br.readLine();

            while (line != null) {
                this.text = this.text + "\n" + line;
                line = br.readLine();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
            br.close();
            System.out.println(this.text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
