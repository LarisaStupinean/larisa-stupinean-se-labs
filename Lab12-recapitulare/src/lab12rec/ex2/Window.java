package lab12rec.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Window extends JFrame{
    private JButton button_writing;
    private JButton button_reading;
    private JTextArea text_write;
    private JTextArea text_read;
    private WriteFile threadWrite;
    private ReadFile threadRead;
    private String writeText=null;

    Window() {
        setTitle("Exercise 1");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(350, 250);
        setLocationRelativeTo(null);
        setLayout(null);
        button_writing = new JButton("Write file");
        button_writing.setBounds(10, 170, 150, 20);
        button_writing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeText = text_write.getText();
                threadWrite = new WriteFile("Thread WriteFile", writeText);
                threadWrite.start();
                text_write.setText(null);
            }
        });

        button_reading = new JButton("Read file");
        button_reading.setBounds(170, 170, 150, 20);
        button_reading.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                threadRead = new ReadFile("Thread ReadFile");
                threadRead.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
                text_read.setText(threadRead.getText());
            }
        });

        text_read = new JTextArea();
        text_read.setBounds(170, 10, 150, 150);

        text_write = new JTextArea();
        text_write.setBounds(10, 10, 150, 150);

        add(button_writing);
        add(button_reading);
        add(text_read);
        add(text_write);
        setVisible(true);
    }
}
