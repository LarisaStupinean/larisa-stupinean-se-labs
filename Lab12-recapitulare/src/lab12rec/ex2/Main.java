package lab12rec.ex2;

//Exercise 2. A GUI with 2 buttons ("Write file", "Read file"). When one of them is pressed a thread is started which
//performs the action suggested by the name of the action, with the mention that each time the button write file is
//pressed, the new content will be overwritten over the old one.


public class Main {
    public static void main(String[] args) {
        new Window();
    }
}
