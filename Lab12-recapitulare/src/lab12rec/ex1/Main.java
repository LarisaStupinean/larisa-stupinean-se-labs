package lab12rec.ex1;

//Exercise 1. A GUI with 2 buttons ("Write file", "Read file") which do exactly what their name suggests,
//with the mention that each time the button write file is pressed, the new content will be overwritten over the old one.

public class Main {
    public static void main(String[] args) {
        new Window();
    }
}
