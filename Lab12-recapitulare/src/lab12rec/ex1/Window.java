package lab12rec.ex1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Window extends JFrame {
    private JButton button_writing;
    private JButton button_reading;
    private JTextArea text_write;
    private JTextArea text_read;

    Window() {
        setTitle("Exercise 1");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(350, 250);
        setLocationRelativeTo(null);
        setLayout(null);
        button_writing = new JButton("Write file");
        button_writing.setBounds(10, 170, 150, 20);
        button_writing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = text_write.getText();
                try {
                    writeFile("Test Exercise 1", text);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                text_write.setText(null);
            }
        });

        button_reading = new JButton("Read file");
        button_reading.setBounds(170, 170, 150, 20);
        button_reading.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = null;
                try {
                    text = readFile("Test Exercise 1");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                text_read.setText(text);
            }
        });

        text_read = new JTextArea();
        text_read.setBounds(170, 10, 150, 150);

        text_write = new JTextArea();
        text_write.setBounds(10, 10, 150, 150);

        add(button_writing);
        add(button_reading);
        add(text_read);
        add(text_write);
        setVisible(true);
    }

    public void writeFile(String fileName, String str) throws IOException {
        File fout = new File(fileName);
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(str);
        bw.newLine();

        bw.close();
    }

    public String readFile(String fileName) throws IOException {
        String text = null;
        File fin = new File(fileName);
        FileInputStream fis = new FileInputStream(fin);

        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line = br.readLine();
        while (line != null) {
            if (text != null) {
                text = text + "\n" + line;
            } else {
                text = line;
            }
            line = br.readLine();
        }
        br.close();
        return text;
    }
}
