package lab10.exercitii.ex4fail2;

import javax.swing.*;
import java.awt.*;

public class Robot extends JPanel{
    // Rectangle robot;
     int positionX;
     int positionY;
     int side = 5;
     Color color;
    //private boolean collided = false;
   // private MovingSpace movingSpace;

//    public Robot(){
//       // this.robot = new Rectangle();
//        setPosition();
//        this.color = new Color(200,0,200);
//       // this.movingSpace = ms;
//    }

    public Color getColor() {
        return color;
    }

        public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        setPosition();
        this.color = new Color(200,0,200);
        g2d.setColor(this.color);
        g2d.fillRect(this.positionX, this.positionY, this.side, this.side);
    }

//    @Override
//    protected void paintComponent (Graphics g){
//        super.paintComponent(g);
//        Graphics2D g2d = (Graphics2D) g.create();
//
////            if (fireBall != null) {
////
////                g2d.setColor(Color.RED);
////                g2d.fill(fireBall);
////
////            }
//
//        g2d.setColor(Color.BLACK);
////        for (Rectangle rct : robots) {
//            g2d.draw(this.robot);
////        }
//        g2d.dispose();
//    }

    public void setPosition(){
        this.positionX = (int) (Math.random() * (99 - 0)+0);
        this.positionY = (int) (Math.random()*(99-0)+0);
       // this.robot.setBounds(positionX, positionY, side, side);
    }

//  //  public Rectangle getRobot(){
//        return robot;
//    }

//    public boolean isCollided() {
//        return collided;
//    }

//    private void doDrawing(Graphics g) {
//
//
//
//        Font font = new Font("Serif", Font.BOLD, 40);
//        g2d.setFont(font);
//
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//                RenderingHints.VALUE_ANTIALIAS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//
//
//    }


//    public void collision(Robot r){
//        this.collided = this.robot.intersects(r.getRobot());
//        if(this.collided)
//            this.robot = null;
//       // return collision;
//    }

//    @Override
//    public void run() {
//        while(!isCollided()){
//            setPosition();
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e){
//                e.printStackTrace();
//            }
//        }
//    }

    public static void main(String[] args) {
        Robot r1 = new Robot();
        Robot r2 = new Robot();
        Robot r3 = new Robot();

//       robots.add(r1);
//       robots.add(r2);
//       robots.add(r3);
        //   MovingSpace movingSpace = new MovingSpace(robots);
        JFrame frame = new JFrame();
        frame.setTitle("Robots simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
//        for(Robot robot: this.robots)
//            add(robot);
        frame.setSize(200,200);
        frame.add(r1);
        frame.add(r2);
        frame.add(r3);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
