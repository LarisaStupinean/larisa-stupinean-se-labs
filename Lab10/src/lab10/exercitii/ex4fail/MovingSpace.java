package lab10.exercitii.ex4fail;

import javax.swing.*;
import java.util.*;

public class MovingSpace extends JFrame {
    ArrayList<Robot> robots = new ArrayList<>();


    public MovingSpace(ArrayList<Robot> robots) {
        this.robots.addAll(robots);
        setTitle("Robots simulation");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        for(Robot robot: robots)
            add(robot.getRobot());
        setSize(200,200);
        setVisible(true);
    }

    public void collision(Robot r) {
        for (Robot robot : robots) {
            //int indexRobot = robots.indexOf(robot);
            if (r.getRobot().getBounds().intersects(robot.getRobot().getBounds())) {
                robots.remove(r);
               // robots.remove(indexRobot);
                r.getRobot().setVisible(false);
               // robot.getRobot().setVisible(false);
            }
        }
    }
}
