package lab10.exercitii.ex4fail;

import java.util.ArrayList;


public class Test {
    public static void main(String[] args) {
        ArrayList<Robot> robots = new ArrayList<Robot>(10);
        ArrayList<Thread> threads = new ArrayList<Thread>(10);
        for (int i = 0; i < 10; i++) {
            Robot robot = new Robot("Robot" + i);
            robots.add(robot);
            threads.add(new Thread(robot, "Thread Robot1"));
        }
        MovingSpace movingSpace = new MovingSpace(robots);
        for (Robot robot : robots)
            robot.setMovingSpace(movingSpace);

        for (Thread thread:threads)
            thread.start();


    }

//        public void paintComponent(Graphics g) {
//            super.paintComponent(g);
//            Graphics2D g2d = (Graphics2D) g;
//
////            g2d.setColor(new Color(212, 100, 212));
////            g2d.drawRect(10, 15, 90, 60);
////
//
//            g2d.setColor(new Color(31, 21, 1));
//            g2d.fillRect(250, 195, 90, 60);
//
//        }
//
//        public static void main(String[] args) {
//            Test rect = new Test();
//            JFrame frame = new JFrame("Rectangles");
//            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//            frame.add(rect);
//            frame.setSize(360, 300);
//            frame.setLocationRelativeTo(null);
//            frame.setVisible(true);
//        }
}
