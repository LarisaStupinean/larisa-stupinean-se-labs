package lab10.exercitii.ex4fail;

import javax.swing.*;

public class Robot implements Runnable{
    private JButton robot;
    MovingSpace movingSpace;
    int side = 5;
    int positionX;
    int positionY;

    public Robot(String name){
        this.robot = new JButton(name);
        setPosition();
//        this.movingSpace.collision(this);

    }

    public void setMovingSpace(MovingSpace movingSpace) {
        this.movingSpace = movingSpace;
    }

    public void setPosition(){
        this.positionX = (int) (Math.random() * (99 - 0));
        this.positionY = (int) (Math.random()*(99-0));
        this.robot.setBounds(positionX, positionY, side, side);
    }

    public JButton getRobot(){
        return robot;
    }


    @Override
    public void run() {
        while(true){
            setPosition();
            movingSpace.collision(this);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
