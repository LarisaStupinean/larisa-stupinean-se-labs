package lab10.exercitii.ex1;

 /*
  We can observe that by replacing start() method with run() method it behaves just like a normal method
  and we are not talking about multithreading anymore. When the run() method gets called though start()
  method, a new thread is being allocated to the execution of run() method; if the run() method is being
  called directly then its execution is being handled by the same current thread and no multithreading
  takes place, so the output reflects the execution of the 3 threads in the specified order.
  */

public class Counter extends Thread{

    Counter(String name){
        super(name);
    }

    public void run(){
        for(int i=0; i<20; i++){
            System.out.println(getName()+" i="+i);
            try {
                Thread.sleep((int)(Math.random()*1000));
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(getName()+" job finalised.");
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("Counter1");
        Counter c2 = new Counter("Counter2");
        Counter c3 = new Counter("Counter3");

//        c1.start();
//        c2.start();
//        c3.start();

        //Replace start() method with run() method

        c1.run();
        c2.run();
        c3.run();
    }

}
