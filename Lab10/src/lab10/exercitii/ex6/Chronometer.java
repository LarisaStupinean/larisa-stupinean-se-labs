package lab10.exercitii.ex6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer extends JFrame {
    Counter time_counter = new Counter();
    JButton start_pause;
    JButton reset;
    JLabel time;
    boolean pause = false;
    int counter_sp = 0;

    Chronometer() {
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 150);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);
        int width = 100;
        int height = 20;

        start_pause = new JButton("Start");
        start_pause.setBounds(50, 80, width, height);

        start_pause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter_sp++;
                if (counter_sp == 1)
                    time_counter.start();
                if(counter_sp%2==0)
                    pause=true;
                else pause = false;
                if(pause)
                    start_pause.setText("Resume");
                else start_pause.setText("Pause");

                synchronized (time_counter){
                    time_counter.notifyAll();
                }
            }
        });

        reset = new JButton("Reset");
        reset.setBounds(240, 80, width, height);

        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                time_counter.hour = 0;
                time_counter.minute = 0;
                time_counter.second = 0;
                if(start_pause.getText().equals("Resume"))
                    start_pause.setText("Start");
                refresh();
            }
        });

        time = new JLabel("00:" + "00:" + "00");
        time.setBounds(170, 30, width, height);

        add(start_pause);
        add(reset);
        add(time);
    }

    public void refresh() {
        String hour, minute, second;

        if (time_counter.hour < 10)
            hour = "0" + String.valueOf(time_counter.hour);
        else hour = String.valueOf(time_counter.hour);

        if (time_counter.minute < 10)
            minute = "0" + String.valueOf(time_counter.minute);
        else minute = String.valueOf(time_counter.minute);

        if (time_counter.second < 10)
            second = "0" + String.valueOf(time_counter.second);
        else second = String.valueOf(time_counter.second);

        time.setText(hour + ":" + minute + ":" + second);
    }

    public static void main(String[] args) {
        new Chronometer();
    }

    class Counter extends Thread {
        int hour = 0, minute = 0, second = 0;

//        private synchronized void count() {
//            pause();
//            second++;
//            if (second == 60) {
//                minute++;
//                second = 0;
//                if (minute == 60) {
//                    hour++;
//                    minute = 0;
//                }
//            }
//        }

        private void pause() {
            synchronized (this) {
                while (pause) {
                    try {
                        this.wait();
                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    pause();
                    second++;
                    if (second == 60) {
                        minute++;
                        second = 0;
                        if (minute == 60) {
                            hour++;
                            minute = 0;
                        }
                    }
                    refresh();
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
