package lab10.exercitii.ex4;

import java.util.*;

public class MovingSpace extends Thread {
    int nr_robots;
    int[] x;
    int[] y;
    ArrayList<Robot> robots;

    MovingSpace(int nr_robots) {
        this.nr_robots = nr_robots;
        this.x = new int[this.nr_robots];
        this.y = new int[this.nr_robots];
        this.robots = new ArrayList<>(this.nr_robots);
        for (int i = 0; i < this.nr_robots; i++) {
            this.robots.add(new Robot("Robot" + (i + 1)));
        }
    }

    public void collision(){
        boolean collision = false;
        for (int i = 0; i < nr_robots; i++) {
            for (int j = i+1; j < nr_robots; j++) {
                collision = x[j] == x[i] && y[j] == y[i] && x[i] != -1;
                if (collision) {
                    x[j] = -1;
                }
            }
            if (collision)
                x[i] = -1;
        }
    }

    public void destroy_robots(){
        for (int i = 0; i < robots.size(); i++) {
            if (x[i] == -1) {
                robots.remove(i);
            }
        }
    }

    @Override
    public void run() {
        for (Robot r : robots) {
            r.move();
            System.out.println(r.getName() + " at position [" + r.getPositionX() + "," + r.getPositionY() + "]");
        }
        while (robots.size() > 1) {
            int i = 0;
            for (Robot r : robots) {
                r.move();
                System.out.println(r.getName() + " at position [" + r.getPositionX() + "," + r.getPositionY() + "]");
                x[i] = r.getPositionX();
                y[i] = r.getPositionY();
                i++;
            }

            collision();
            destroy_robots();

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Simulation over!");
    }

    public static void main(String[] args) {
        MovingSpace movingSpace = new MovingSpace(10);
        movingSpace.start();
    }
}
