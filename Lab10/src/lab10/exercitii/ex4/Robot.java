package lab10.exercitii.ex4;

import java.util.Random;

public class Robot {
    private String name;
    private int positionX;
    private int positionY;

    Robot(String name){
        this.name = name;
    }

    public int randomNr(int min, int max) {
        return (int) (min + Math.random() * ((max - min) + 1));
    }

    public void move(){
        this.positionX = randomNr(1,100);
        this.positionY = randomNr(1,100);
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public String getName(){
        return name;
    }
}
