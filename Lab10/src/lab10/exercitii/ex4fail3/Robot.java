package lab10.exercitii.ex4fail3;

import java.awt.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Robot implements Runnable {
    Rectangle robot;
    int positionX, positionY;
    int side = 5;
    Color color;
    boolean collided = false;
    MovingSpace ms;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public Robot(MovingSpace ms) {
        this.positionX = randomNr(0, 99);
        this.positionY = randomNr(0, 99);
        this.color = new Color(randomNr(0, 200), randomNr(0, 200), randomNr(0, 200));
        this.robot = new Rectangle(positionX, positionY, side, side);
        this.ms = ms;
    }

    public int randomNr(int min, int max) {
        return (int) (min + Math.random() * ((max - min) + 1));
    }

    public void setPosition() {
        this.positionX = randomNr(0, 99);
        this.positionY = randomNr(0, 99);
        this.robot.setBounds(positionX, positionY, side, side);
    }

    public Rectangle getRobot() {
        return robot;
    }

    public void destroy(){
        if (this.collided)
            this.robot = null;
    }

    public void stop() {
        running.set(false);
    }


    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread was interrupted, Failed to complete operation");
            }
            ms.collision(this);
            while (!this.collided) {
                setPosition();
                ms.repaint(this.robot);
                ms.collision(this);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (this.collided) {
                stop();
               // destroy();
             //   ms.repaint(robot);
            }
        }
    }
}

