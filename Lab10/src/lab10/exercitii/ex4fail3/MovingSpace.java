package lab10.exercitii.ex4fail3;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MovingSpace extends JPanel{
    ArrayList<Robot> robots = new ArrayList<>(10);
    ArrayList<Thread> threads = new ArrayList<>(10);

    public MovingSpace(){
        for (int i=0; i<10; i++){
            Robot r = new Robot(this);
            this.robots.add(r);
            Thread t = new Thread(r,"Thread Robot "+i);
            this.threads.add(t);
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        for (Robot r:robots) {
            g2d.setColor(r.color);
            g2d.fill(r.getRobot());
        }
    }

    public void collision(Robot robot) {
//        for (Robot checkRobot : this.robots) {
            for (Robot r : this.robots) {
                robot.collided = robot.getRobot().intersects(r.getRobot());
                if(robot.collided)
                    this.robots.remove(robot);
//                  //  robot = null;
//            }
        }
    }

//    public void refresh(Robot robot){
////            for (Robot r : this.robots) {
//                if(robot.changedPosition)
//                    repaint();
////            }
//    }

    public static void main(String[] args) {
        MovingSpace ms = new MovingSpace();
        JFrame frame  = new JFrame();
        frame.setTitle("Robots Simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Color.WHITE);
        frame.add(ms);
        frame.setSize(300,300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
//
        for (Thread t : ms.threads)
            t.start();

//        for(Robot r : ms.robots){
//            Thread t = new Thread(r);
//            t.start();
//        }
    }
}
