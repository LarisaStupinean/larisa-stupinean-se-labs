package lab10.exercitii.ex2;

 /*
  In this program multiple threads try to access methods from class Punct. When synchronized blocks are not used
  the result of the program is erroneous. When synchronized blocks are used only one thread can access the
  resource at a given point of time, and the behaviour of the application is the foreseen one and the FirGet fg1
  "reads" points "written" by FirSet fs1.
  */

public class TestSincronizare {
    public static void main(String[] args) {
        Punct p = new Punct();
        FirSet fs1 = new FirSet(p);
        FirGet fg1 = new FirGet(p);

        fs1.start();
        fg1.start();
    }
}
