package lab10.exercitii.ex3;

public class Counter extends Thread {
    int limMin;
    int limMax;

    Counter(String name, int limMin, int limMax) {
        super(name);
        this.limMin = limMin;
        this.limMax = limMax;
    }

    public void run() {
        System.out.println(getName() + " entered run() method.");
        for (int i = limMin; i <= limMax; i++) {
            System.out.println(getName() + " i=" + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}
