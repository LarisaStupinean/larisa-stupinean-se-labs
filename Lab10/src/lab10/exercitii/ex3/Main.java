package lab10.exercitii.ex3;

public class Main {
    public static void main(String[] args) {
        Counter counter1 = new Counter("Counter1", 0, 100);
        Counter counter2 = new Counter("Counter2", 100, 200);

        counter1.start();

        try {
            counter1.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        counter2.start();
    }
}
