package lab10.exemple.exemplu6;

class Robot {
    private final String name;
    Piesa piesa;

    public Robot(String name){
        this.name = name;
        this.piesa = new Piesa();
    }

    public String getName(){
        return this.name;
    }

    public synchronized void proceseazaPiesa(Robot r){
        System.out.println(name+" proceseaza piesa.");
        piesa.procesare();
        r.primestePiesa(this);
    }

    public synchronized void primestePiesa(Robot r){
        System.out.println(r.getName()+" a trimis piesa catre "+name);
        this.piesa = r.getPiesa();
    }

    private Piesa getPiesa(){
        return piesa;
    }
}
