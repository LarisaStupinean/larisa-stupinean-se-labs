package lab10.exemple.exemplu6;

//Interblocaje (deadlocks)

public class Deadlock {

    public static void main(String[] args) {
        final Robot alphonse = new Robot("Alphonse");
        final Robot gaston = new Robot("Gaston");
        new Thread(new Runnable(){
            public void run(){
                alphonse.proceseazaPiesa(gaston);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                gaston.proceseazaPiesa(alphonse);
            }
        }).start();
    }
}
