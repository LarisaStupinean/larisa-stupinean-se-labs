package lab10.exemple.exemplu7;

import java.util.ArrayList;

public class Buffer {
    //Vector folosit pt a inmagazina oobiecte de tip Double
    ArrayList content = new ArrayList();

    /*
    Prin intermediul acestei metode sunt adaugate elemente in containerul content.
    Se observa ca aceasta metoda este sincronizata. Metoda va fi apelata de firele
    de executie de tip Producer.

    Dupa adaugarea unui element in container se apeleaza metoda notify(); aceasta
    asigura trezirea unui fir de executie ce a fost blocat prin apelul functiei wait().
    @param d
     */
    synchronized void push(double d){
        content.add(new Double(d));
        notify();
    }

    /*
    Aceasta metoda e folosita pentru a extrage elemente din cadrul containerului
    content. Se observa ca aceasta metoda este sincronizata.
    Daca containerul este gol se apeleaza metoda wait(). Aceasta va bloca firul
    de executie apelant pana in momentul in care un fir de executie producator
    ca adauga in container un element si va apela metoda notify().

    @return
     */

    synchronized double get(){
        double d = -1;
        try {
            while (content.size()==0)
                wait();
            d=(((Double)content.get(0))).doubleValue();
            content.remove(0);
        } catch (Exception e){
            e.printStackTrace();
        }
        return d;
    }
}
