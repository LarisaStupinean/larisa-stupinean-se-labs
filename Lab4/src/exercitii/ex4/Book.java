package lab4.ex4;

import lab4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name, Author[] authors, double price){
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = 0;
    }
    public Book(String name, Author[] authors, double price, int qtyInStock){
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName(){
        return name;
    }
    public Author[] getAuthors(){
        return authors;
    }
    public double getPrice(){
        return price;
    }
    public void setPrice(double price){
        this.price = price;
    }
    public int getQtyInStock(){
        return qtyInStock;
    }
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock = qtyInStock;
    }
    public String toString(){
        //Author[] authors = this.getAuthors();
        return this.getName()+" by "+this.getAuthors().length+" authors";
    }
    public void printAuthors(){
        int no_aut = this.getAuthors().length;
        System.out.println("The authors of the book are: ");
        for(int i=0; i<no_aut; i++) {
            System.out.println("\t"+this.getAuthors()[i].toString());
        }
    }
}
