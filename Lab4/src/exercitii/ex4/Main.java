package lab4.ex4;

import lab4.ex2.Author;

public class Main {
    public static void main(String[] args) {
        Author[] authors1 = new Author[2];
        authors1[0] = new Author("Maria Pop","mp@hmail.com",'f');
        authors1[1] = new Author("Vlad Nistor","vn@gmail.com",'m');
        Book b1 = new Book("Fericire", authors1, 40 );

        Author[] authors2 = new Author[3];
        authors2[0] = new Author("Mircea Dan", "md@gmail.com", 'm');
        authors2[1] = new Author("Laura Stroe", "ls@gmail.com", 'f');
        authors2[2] = new Author("Diana Pop", "dp@gmail.com", 'f');
        Book b2 = new Book("Psihologia vietii de zi cu zi", authors2, 45,1500 );


        //testam metodele getName() si getAuthors() at. cand testam metodele toString() si printAuthors()
        System.out.println(b1.toString());
        b1.printAuthors();
        System.out.println("The price of the book is "+b1.getPrice());
        System.out.println("The quantity in stock is "+b1.getQtyInStock()+"\n");

        System.out.println(b2.toString());
        b2.printAuthors();
        System.out.println("The initial price of the book is "+b2.getPrice());
        b2.setPrice(25);
        System.out.println("The new price of the book is "+b2.getPrice());
        System.out.println("The initial quantity in stock is "+b2.getQtyInStock());
        b2.setQtyInStock(1000);
        System.out.println("The new quantity in stock is "+b2.getQtyInStock());
    }
}
