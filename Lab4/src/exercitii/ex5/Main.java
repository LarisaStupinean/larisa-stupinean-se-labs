package lab4.ex5;

public class Main {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder();
        Cylinder c2 = new Cylinder(3);
        Cylinder c3 = new Cylinder(5, 10);
        System.out.println("Details for cylinder c1: \n\tColor: "+c1.getColor()+"\n\tRadius: "+c1.getRadius()+
                "\n\tHeight: "+c1.getHeight()+"\n\tArea: "+c1.getArea()+"\n\tVolume: "+c1.getVolume());
        System.out.println("\nDetails for cylinder c2: \n\tColor: "+c2.getColor()+"\n\tRadius: "+c2.getRadius()+
                "\n\tHeight: "+c2.getHeight()+"\n\tArea: "+c2.getArea()+"\n\tVolume: "+c2.getVolume());
        System.out.println("\nDetails for cylinder c3: \n\tColor: "+c3.getColor()+"\n\tRadius: "+c3.getRadius()+
                "\n\tHeight: "+c3.getHeight()+"\n\tArea: "+c3.getArea()+"\n\tVolume: "+c3.getVolume());
    }
}
