package lab4.ex5;

import lab4.ex1.Circle;

public class Cylinder extends Circle {
    double height = 1.0;

    public Cylinder(){
        super();
    }
    public Cylinder(double radius){
        super(radius);
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight(){
        return height;
    }
    public double getVolume(){
        return Math.PI*this.getRadius()*this.getRadius()*height;
    }

    @Override
    public double getArea() {
        return 2*Math.PI*this.getRadius()*(this.getRadius()+height);
    }
}
