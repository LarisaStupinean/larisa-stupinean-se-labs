package lab4.ex1;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle(){}
    public Circle(double radius){
        this.radius = radius;
        color = "black";
    }

    public String getColor(){
        return color;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
