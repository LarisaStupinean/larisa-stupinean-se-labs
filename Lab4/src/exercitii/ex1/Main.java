package lab4.ex1;

public class Main {

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(10);
        System.out.println("For c1: ");
        System.out.println("The color of the circle is "+c1.getColor());
        System.out.println("The radius of the circle is "+c1.getRadius());
        System.out.println("The area of the circle is "+c1.getArea());

        System.out.println("\nFor c2: ");
        System.out.println("The color of the circle is "+c2.getColor());
        System.out.println("The radius of the circle is "+c2.getRadius());
        System.out.println("The area of the circle is "+c2.getArea());
    }
}