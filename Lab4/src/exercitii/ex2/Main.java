package lab4.ex2;

public class Main {
    public static void main(String[] args) {
    //gender poate fi doar 'm' sau 'f'
    Author b = new Author("Maria Pop","maria.pop@gmail.com",'c');

    Author a = new Author("Mircea Cartarescu","m.cartarescu@gmail.com",'m');
    //testam functionarea corecta a metodelor de tip getX() prin testarea metodei toString()
    System.out.println(a.toString());
    a.setEmail("mircea.cartarescu@yahoo.com");
    System.out.println("Dupa modificari:\n"+a.toString());
    }
}
