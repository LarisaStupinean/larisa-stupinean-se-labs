package lab4.ex6;

public class Square extends Rectangle {
    public Square(){
        super();
    }
    public Square(double side){
        super(side,side);
    }
    public Square(double side, String color, boolean filled){
        super(side, side, color, filled);
    }
    public double getSide(){
        return width;
    }
    public void setSide(double side){
        this.width = this.length = side;
    }
    public void setWidth(double side){
        super.setWidth(side);
        this.length = side;
    }
    public void setLength(double side){
        super.setLength(side);
        this.width = side;
    }
    public String toString(){
        return "A Square with side="+this.getSide()+", which is a subclass of "+super.toString();
    }
}
