package lab4.ex6;

public class Main {
    public static void main(String[] args) {
        Shape sh1 = new Shape();
        Shape sh2 = new Shape("pink", false);
        //metodele getColor() si isFilled() sunt testate odata cu testarea metodei toString()
        System.out.println("For sh1:\t"+sh1.toString());
        //modificam atributele
        sh1.setColor("blue");
        sh1.setFilled(false);
        System.out.println("After changes:\t"+sh1.toString());
        System.out.println("For sh2:\t"+sh2.toString());

        Circle c1 = new Circle();
        Circle c2 = new Circle(5);
        Circle c3 = new Circle(10,"green",true);
        //metodele getColor(), getRadius() si isFilled() sunt testate odata cu testarea metodei toString()
        System.out.println("\nFor c1:\t"+c1.toString());
        System.out.println("The area is "+c1.getArea()+" and the perimeter is "+c1.getPerimeter());
        //modificam atributele
        c1.setRadius(10);
        c1.setColor("magenta");
        c1.setFilled(false);
        System.out.println("After changes:\t"+c1.toString());
        System.out.println("For c2:\t"+c2.toString());
        System.out.println("For c3: "+c3.toString());

        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(5, 10);
        Rectangle r3 = new Rectangle(4, 3, "white",false);
        //metodele getColor(), getWidth(), getLength() si isFilled() sunt testate odata cu testarea metodei toString()
        System.out.println("\nFor r1: "+r1.toString());
        System.out.println("The area is "+r1.getArea()+" and the perimeter is "+r1.getPerimeter());
        //modificam atributele
        r1.setLength(6);
        r1.setWidth(7);
        r1.setColor("blue");
        r1.setFilled(false);
        System.out.println("After changes:"+r1.toString());
        System.out.println("The area is "+r1.getArea()+" and the perimeter is "+r1.getPerimeter());
        System.out.println("For r2: "+r2.toString());
        System.out.println("For r3: "+r3.toString());

        Square s1 = new Square();
        Square s2 = new Square(5);
        Square s3 = new Square(4, "yellow",false);
        //metodele getColor(), getSide() si isFilled() sunt testate odata cu testarea metodei toString()
        System.out.println("\nFor s1: "+s1.toString());
        System.out.println("The area is "+s1.getArea()+" and the perimeter is "+s1.getPerimeter());
        //modificam atributele
        s1.setSide(8);
        s1.setColor("green");
        s1.setFilled(false);
        System.out.println("After changes:"+s1.toString());
        System.out.println("The area is "+s1.getArea()+" and the perimeter is "+s1.getPerimeter());
        System.out.println("For s2: "+s2.toString());
        s2.setWidth(9);
        System.out.println("After changes:"+s2.toString());
        System.out.println("For s3: "+s3.toString());
        s3.setLength(20);
        System.out.println("After changes:"+s3.toString());
    }
}
