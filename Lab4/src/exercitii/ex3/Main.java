package lab4.ex3;

import lab4.ex2.Author;

public class Main {
    public static void main(String[] args) {
    Book b1 = new Book("Gone Girl", new Author("Gillian Flynn","gf@hmail.com",'f'),50);
    Book b2 = new Book("Pnin", new Author("Vladimir Nabokov", "vn@yahoo.com",'m'),35,1000 );
    //testam metodele getName() si getAuthor() at. cand testam metoda toString()
    System.out.println(b1.toString());
    System.out.println("The price of the book is "+b1.getPrice());
    System.out.println("The quantity in stock is "+b1.getQtyInStock());

    System.out.println("\n"+b2.toString());
    System.out.println("The initial price of the book is "+b2.getPrice());
    b2.setPrice(25);
    System.out.println("The new price of the book is "+b2.getPrice());
    System.out.println("The initial quantity in stock is "+b2.getQtyInStock());
    b2.setQtyInStock(593);
    System.out.println("The new quantity in stock is "+b2.getQtyInStock());
    }
}
