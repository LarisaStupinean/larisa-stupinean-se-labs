package lab6.exercitii.ex4;

import java.io.*;

public class ConsoleMenu {
    public static void main(String[] args) throws Exception {
        Dictionary dictionary = new Dictionary();
        char choice;
        String line, def;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.println("\nMenu");
            System.out.println("a - Add word");
            System.out.println("g - Get definition of a word");
            System.out.println("w - Get all words");
            System.out.println("d - Get all definitions");
            System.out.println("s - Display dictionary");
            System.out.println("e - Exit");

            line = fluxIn.readLine();
            choice = line.charAt(0);

            switch (choice) {
                case 'a':
                case 'A':
                    System.out.println("Introduce word: ");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        System.out.println("Introduce definition: ");
                        def = fluxIn.readLine();
                        dictionary.addWord(new Word(line), new Definition(def));
                    }
                    break;

                case 'g':
                case 'G':
                    System.out.println("Introduce word: ");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        Word w = new Word(line);
                        Definition d = dictionary.getDefinition(w);
                        if (d != null)
                            System.out.println("Definition: " + d);
                        else System.out.println("Word \""+w+"\" not found!");
                    }
                    break;
                case 'w':
                case 'W':
                    System.out.println("All words in dictionary: ");
                    System.out.println(dictionary.getAllWords());
                    break;
                case 'd':
                case 'D':
                    System.out.println("All definitions in dictionary: ");
                    System.out.println(dictionary.getAllDefinitions());
                    break;
                case 's': case 'S':
                    System.out.println("Displaying dictionary: ");
                    dictionary.displayDictionary();
                    break;
                default:
                    if(choice!='e'&&choice!='E')
                        System.out.println("Invalid choice!");
            }
        } while (choice!='e'&&choice!='E');
        System.out.println("Program finished.");
    }
}
