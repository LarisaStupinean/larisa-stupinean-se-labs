package lab6.exercitii.ex2;

public class BankAccount implements Comparable{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        if(this.balance >= amount) {
            this.balance = this.balance - amount;
            System.out.println("Amount withdrawn successfully!");
        }
        else System.out.println("Insuficient amount!");
    }
    public void deposit(double amount){
        this.balance=this.balance+amount;
        System.out.println("Amount added successfully!");
    }

    public double getBalance(){
        return this.balance;
    }

    public String getOwner(){
        return this.owner;
    }

    public boolean equals(Object o){
        if(o==null || !(o instanceof BankAccount))
            return false;
        BankAccount x =(BankAccount)o;
        return x.owner.equalsIgnoreCase(this.owner)&&x.balance==this.balance;
    }
    public int hashCode(){
        return (int) (owner.length()*1000);
    }

    public int compareTo(Object o) {
        BankAccount bA = (BankAccount)o;
        if(this.balance>bA.balance) return 1;
        else if(this.balance==bA.balance) return 0;
        else return -1;
    }

    public String toString(){
        return "Account(owner: "+this.owner+"; balance: "+this.balance+")";
    }
}
