package lab6.exercitii.ex3;

import java.util.*;

public class Bank {
    private TreeSet<BankAccount> bankAccounts1 = new TreeSet<BankAccount>(new CompByBalance());
    private TreeSet<BankAccount> bankAccounts2 = new TreeSet<BankAccount>(new CompByOwner());

    public void addAccount(String owner, double balance) {
        BankAccount bA = new BankAccount(owner, balance);
        bankAccounts1.add(bA);
        bankAccounts2.add(bA);
    }

    public void printAccounts() {
        System.out.println("The accounts sorted by balance: ");
        System.out.println(bankAccounts1);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        System.out.println("The accounts with the balance between " + minBalance + " and " + maxBalance + ": ");
        Iterator it = bankAccounts1.iterator();
        while (it.hasNext()) {
            BankAccount bA = (BankAccount) it.next();
            if (bA.getBalance() >= minBalance && bA.getBalance() <= maxBalance)
                System.out.println(bA);
        }
    }

    public BankAccount getAccount(String owner) {
        boolean check;
        BankAccount acc = null;
     Iterator it = bankAccounts2.iterator();
        while (it.hasNext()) {
            BankAccount bA = (BankAccount) it.next();
            check = bA.getOwner().equalsIgnoreCase(owner);
            if (check)
                acc = bA;
        }
        return acc;
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return this.bankAccounts2;
    }
}

