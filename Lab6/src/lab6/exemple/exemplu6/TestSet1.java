package lab6.exemple.exemplu6;

import java.util.*;

public class TestSet1 {
    static void displayAll(Set list){
        System.out.println("- - - - - - - -");
        Iterator i = list.iterator();
        while(i.hasNext()){
            String s = (String)i.next();
            System.out.println(s);
        }
    }

    //@param args
    public static void main(String[] args) {
        HashSet set1 = new HashSet();
        set1.add("one"); set1.add("two"); set1.add("six"); set1.add("six");
        set1.add("one"); set1.add("four"); set1.add("five");
        displayAll(set1);

        TreeSet tree = new TreeSet();
        tree.add("one"); tree.add("two"); tree.add("six"); tree.add("six");
        tree.add("one"); tree.add("four"); tree.add("five");
        displayAll(tree);

        LinkedHashSet lnk = new LinkedHashSet();
        lnk.add("one"); lnk.add("two"); lnk.add("six"); lnk.add("six");
        lnk.add("one"); lnk.add("four"); lnk.add("five");
        displayAll(lnk);
    }
}
