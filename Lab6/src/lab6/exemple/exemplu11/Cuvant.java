package lab6.exemple.exemplu11;

class Cuvant {
    String c;

    public Cuvant(String c){
        this.c=c;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Cuvant))
            return false;
        Cuvant x = (Cuvant)obj;
        return c.equals(x.c);
    }
    public int hashCode(){
        return (int)(c.length()*1000);
    }
    public String toString(){
        return c;
    }
}
