package lab6.exemple.exemplu7;

class Person implements Comparable{
    int age;
    String name;

    Person(String n, int a){
        age = a;
        name = n;
    }

    public int compareTo(Object o){
        Person p = (Person)o;
        if(age>p.age) return 1;
        if(age==p.age) return 0;
        return -1;
    }
    public String toString(){
        return "("+name+":"+age+")";
    }
}
