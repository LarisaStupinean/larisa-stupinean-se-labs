package lab6.exemple.exemplu3;

//exemplu metoda toString()
public class Point {
    private int x;
    private int y;

    public static void main(String[] args) {
        Point p1 = new Point();
        Point p2 = new Point();
        System.out.println("p1="+p1);
        System.out.println("p2="+p2);
        p2=p1;
        System.out.println("p2="+p2);
        Point p3 = null;
        System.out.println("p3="+p3);
    }
}
