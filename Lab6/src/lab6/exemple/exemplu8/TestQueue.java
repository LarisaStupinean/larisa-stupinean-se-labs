package lab6.exemple.exemplu8;

import java.util.PriorityQueue;

public class TestQueue {
    public static void main(String[] args) {
        Job j1 = new Job("check trains on input rail segments",3);
        Job j2 = new Job("check trains on output rail segments",2);
        Job j3 = new Job("check trains on rail station segments",1);

        PriorityQueue que = new PriorityQueue();
        que.offer(j1);
        que.offer(j2);
        que.offer(j3);

        while(que.size()!=0){
            Job j = (Job)que.poll();
            j.execute();
        }
    }
}
