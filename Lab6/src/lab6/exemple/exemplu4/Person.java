package lab6.exemple.exemplu4;

class Person {
    String firstname;
    String lastname;

    Person(String f, String l){
        this.firstname=f;
        this.lastname=l;
    }

    public boolean equals(Object obj){
        if(obj instanceof Person){
            Person p = (Person)obj;
            return p.firstname.equalsIgnoreCase(firstname)&&p.lastname.equalsIgnoreCase(lastname);
        }
        else return false;
    }
    public String toString(){
        return "persoana: "+firstname+":"+lastname;
    }
}
