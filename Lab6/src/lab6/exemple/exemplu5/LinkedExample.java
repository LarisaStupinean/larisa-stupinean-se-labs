package lab6.exemple.exemplu5;

import java.util.*;

public class LinkedExample {
    public static void main(String[] args) {
        LinkedList lk = new LinkedList();
        lk.addFirst(new Command("comanda 1"));
        lk.addFirst(new Command("comanda 2"));
        lk.addFirst(new Command("comanda 3"));

        Command c = (Command)lk.removeLast();
        c.execute();
        c = (Command)lk.removeLast();
        c.execute();
        c = (Command)lk.removeLast();
        c.execute();
    }
}
