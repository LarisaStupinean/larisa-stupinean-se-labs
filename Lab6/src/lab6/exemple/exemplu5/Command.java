package lab6.exemple.exemplu5;

class Command {
    String name;

    Command(String n){
        name = n;
    }

    void execute(){
        System.out.println("Execute command: "+name);
    }
}
