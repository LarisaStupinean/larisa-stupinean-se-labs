package lab2.ex2;
import java.util.*;

public class PrintNumberInWord {
    public static void main(String[] args){
        int number=7;
        String number_str;
        //nested-if
        /*if(number<=9){
            if(number==1) System.out.println("ONE");
            else if(number==2) System.out.println("TWO");
            else if(number==3) System.out.println("THREE");
            else if(number==4) System.out.println("FOUR");
            else if(number==5) System.out.println("FIVE");
            else if(number==6) System.out.println("SIX");
            else if(number==7) System.out.println("SEVEN");
            else if(number==8) System.out.println("EIGHT");
            else if(number==9) System.out.println("NINE");
        } else System.out.println("OTHER");*/

        //switch case
        switch(number) {
            case 1:
                number_str = "ONE";
                break;
            case 2:
                number_str = "TWO";
                break;
            case 3:
                number_str="THREE";
                break;
            case 4:
                number_str="FOUR";
                break;
            case 5:
                number_str="FIVE";
                break;
            case 6:
                number_str="SIX";
                break;
            case 7:
                number_str="SEVEN";
                break;
            case 8:
                number_str="EIGHT";
                break;
            case 9:
                number_str="NINE";
                break;
            default:
                number_str="OTHER";
                break;
        }
        System.out.println(number_str);
    }
}
